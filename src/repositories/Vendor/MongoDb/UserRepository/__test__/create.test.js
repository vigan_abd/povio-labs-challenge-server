const { expect } = require('chai');
const User = require('../../../../../models/Domain/User');

module.exports = (container) => {
  const repository = container.resolve('iuserRepository');

  it("Test success create case", async () => {
    const user = await repository.create(new User({
      username: "test",
      email: "test@mail.com",
      password: "asfohasohuioedshuthetes9ydgnsdbngi"
    }));
    return expect(user.id).to.exist;
  });

  after(async () => {
    await repository.remove({});
  });
};
