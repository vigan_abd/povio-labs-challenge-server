const seed = require('./seed');
const IUserRepository = require('../../../Interfaces/IUserRepository');
const UserDefinedException = require("../../../../models/Business/Exeption/UserDefinedException");

const User = require('../../../../models/Domain/User');

const users = seed();

class UserRepository extends IUserRepository {
  /**
   * 
   * @param {User} model
   */
  async create(model) {
    await Promise.resolve();
    model.id = users.length + 1;
    users.push(model);
    return model;
  }

  /**
   * @param {String} id
   */
  async findById(id) {
    await Promise.resolve();
    const user = users.find(x => x.id == id);
    if (!user) throw new UserDefinedException("User doesn't exist", 404);
    return user;
  }

  /**
   * @param {*} conditions
   */
  async findWhere(conditions) {
    await Promise.resolve()
    return users;
  }

  /**
   * @param {Number} page
   * @param {*} conditions
   * @param {*} options
   */
  async list(page, conditions = {}, options = {}) {
    await Promise.resolve();
    return {
      docs: users,
      totalDocs: users.length,
      limit: users.length,
      hasPrevPage: false,
      hasNextPage: false,
      page: 1,
      totalPages: 1,
      prevPage: null,
      nextPage: null
    }
  }

  /**
   * @param {*} conditions
   */
  async count(conditions = null) {
    await Promise.resolve();
    return users.length;
  }

  /**
   * @param {String} id 
   * @param {*} fields 
   */
  async update(id, fields) {
    await Promise.resolve();
    const index = users.findIndex(x => x.id == id);
    if (index < 0) throw new UserDefinedException("User doesn't exist", 404);
    for (const key in fields) {
      users[index][key] = fields[key];
    }
    return users[index];
  }

  /**
   * @param {*} conditions 
   * @returns {Promise<Boolean>}
   */
  async remove(conditions) {
    await Promise.resolve();
    users.pop();
    return true;
  }

  reset() {
    users = seed();
  }
}

module.exports = UserRepository;