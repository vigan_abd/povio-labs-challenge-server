
const User = require('../../../../models/Domain/User');

module.exports = () => [
  new User({
    "id": "5c085c669b12c7002a8321eb",
    "username": "vigan2",
    "email": "vigan2@mail.com",
    "password": null,
    "tokenHash": null,
    "passwordResetToken": null,
    "passwordResetSentAt": null,
    "userLikes": [],
    "totalLikes": 0,
    "created": "2018-12-05T23:16:54.829Z"
  }),
  new User({
    "id": "5c071b97dcf6820403f6d97b",
    "username": "vigan.abd",
    "email": "vig.an.abd@gmail.com",
    "password": null,
    "tokenHash": null,
    "passwordResetToken": null,
    "passwordResetSentAt": null,
    "userLikes": [
      "5c085c669b12c7002a8321eb"
    ],
    "totalLikes": 1,
    "created": "2018-12-05T00:28:07.533Z"
  })
];