class ILoggerService {

  /**
   * @param {String} type
   * @param {any} message
   * @param {any} tags
   */
  log(type, message, tags) {
    const { logger } = this;

    logger.log(type, message, tags);
  }
};


module.exports = ILoggerService;