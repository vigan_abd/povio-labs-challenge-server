const winston = require('winston');
const ILoggerService = require('../index');
const config = require('../../../config');

class ConsoleLoggerService extends ILoggerService {

  constructor() {
    super();
    const logger = new winston.Logger({
      transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
          timestamp: true,
          colorize: true,
        })
      ]
    });

    logger.on('error', err => {
      console.log(err);
    });

    logger.level = config.LOG_LEVEL;

    logger.stream = {
      write: message => {
        logger.info(message);
      }
    };

    this.logger = logger;
    this.stream = logger.stream;
    this.log = this.log.bind(this);
  }

  /**
   * @param {String} type
   * @param {any} message
   * @param {any} tags
   */
  log(type, message, tags) {
    const { logger } = this;

    logger.log(type, message, tags);
  }
};


module.exports = ConsoleLoggerService;