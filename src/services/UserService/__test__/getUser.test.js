const { expect } = require('chai');
const UserRepository = require('../../../repositories/Vendor/Test/UserRepository');

module.exports = (container) => {
  const service = container.resolve('userService');
  const repository = container.resolve('iuserRepository');

  before(() => {
    // Replace repository with mockup
    service.iuserRepository = new UserRepository();
  })

  it("Test success getUser case", async () => {
    const user = await service.getUser("5c085c669b12c7002a8321eb");
    return expect(user.id).to.exist;
  });

  after(() => {
    // Replace repository mockup with real one
    service.iuserRepository = repository;
  });
};
