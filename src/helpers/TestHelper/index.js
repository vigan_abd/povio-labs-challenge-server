const fs = require('fs');
const path = require('path');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');
const EventEmitter = require('events').EventEmitter;

class TestHelper {
  static createExpressMocks(reqOptions = {}, resOptions = {}, next) {
    resOptions.eventEmitter = EventEmitter;
    const { req, res } = httpMocks.createMocks(reqOptions, resOptions);

    return {
      req,
      res,
      next: (sinon.spy() || next)
    };
  }

  /**
   * @param {String} name 
   * @param {String} path 
   * @param {*} opts 
   */
  static importTest(name, path, opts) {
    describe(name, () => {
      const test = require(path);
      if (opts && typeof test === "function") test(opts);
    });
  };

  static executeTestsInDir(dirname, opts, prefix = "") {
    const files = fs.readdirSync(dirname);
    files.forEach(file => {
      if (file !== 'index.test.js') {
        TestHelper.importTest(
          `${prefix}${file.replace(".test.js", "")}`,
          path.join(dirname, file),
          opts
        );
      }
    });
  }
}

module.exports = TestHelper
