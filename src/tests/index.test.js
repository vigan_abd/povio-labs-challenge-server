const container = require('../containers/test');
const dbAdapter = require('../helpers/DbAdapter');
const TestHelper = require('../helpers/TestHelper');

global.TestHelper = TestHelper;

// Initialization point
before(() => {
  console.log("*** Starting tests! ***");
});


// Tests

// Unit tests
describe("*** Unit testing! ***", () => {
  const repositories = [
    "Vendor/MongoDb/UserRepository",
  ];

  repositories.forEach(r => TestHelper.importTest(
    `${r} tests`,
    `${__dirname}/../repositories/${r}/__test__/index.test`,
    container
  ));

  const services = [
    "UserService",
  ];

  services.forEach(s => TestHelper.importTest(
    `${s} tests`,
    `${__dirname}/../services/${s}/__test__/index.test`,
    container
  ));
});


// Integration tests
describe("*** Integration testing! ***", () => {
  const controllers = [
    'UserAPIController',
    'AuthenticationAPIController',
  ];

  controllers.forEach(c => TestHelper.importTest(
    `${c} tests`,
    `${__dirname}/../controllers/api/${c}/__test__/index.test`,
    container
  ));
});


// Desctruction point
after(() => {
  console.log("*** Tests ended, disposing resources! ***");
  container.dispose();
  dbAdapter.disconnect();
});