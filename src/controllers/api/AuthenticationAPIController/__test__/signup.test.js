const { expect } = require('chai');

const testUserData = {
  email: "test@gmail.com",
  password: "abcd1234",
  username: "test"
};

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('authenticationAPIController');

  before(async () => {
    await service.deleteUsers({});
  });

  it("Test successfull signup case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: testUserData });

    await controller.signup(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test signup email/username exists case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: testUserData });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup email/username exists case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: testUserData });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup missign params case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: {} });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup password numbers only case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: { password: "12345678" } });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup password letters only case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: { password: "abcdefgh" } });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup password illegal chars case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: { password: " 1a  @@@@" } });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup password wrong lenght case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: { password: "abcd123" } });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test signup invalid email case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: { password: "abcd1234", email: "test", username: "test2" } });

    await controller.signup(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
