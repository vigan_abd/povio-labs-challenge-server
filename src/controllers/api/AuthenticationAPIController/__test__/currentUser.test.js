const { expect } = require('chai');

const seed = {
  email: "test@gmail.com",
  password: "abcd1234",
  username: "test"
};
let user = null;

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('authenticationAPIController');

  before(async () => {
    await service.deleteUsers({});
    const auth = await service.signup({ body: seed });
    user = await service.getUser(auth.userId);
  });

  it("Test authenticated request case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ currentUser: user });

    await controller.currentUser(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test unauthenticated request case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ });

    await controller.currentUser(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
