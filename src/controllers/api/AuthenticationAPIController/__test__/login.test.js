const { expect } = require('chai');

const testUserData = {
  email: "test@gmail.com",
  password: "abcd1234",
  username: "test"
};

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('authenticationAPIController');

  before(async () => {
    await service.deleteUsers({});
    await service.signup({ body: testUserData });
  });

  it("Test successfull login with username case", async () => {
    const { email, ...body } = testUserData;
    const { req, res, next } = TestHelper.createExpressMocks({ body });

    await controller.signin(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test successfull login with email case", async () => {
    const { email, ...body } = testUserData;
    body.username = email;
    const { req, res, next } = TestHelper.createExpressMocks({ body });

    await controller.signin(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test login missign params case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ body: {} });

    await controller.signin(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test login invalid password case", async () => {
    const { email, ...body } = testUserData;
    body.password = "12345678";
    const { req, res, next } = TestHelper.createExpressMocks({ body });

    await controller.signin(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test login inexistent user case", async () => {
    const { email, ...body } = testUserData;
    body.username = "notexist";
    const { req, res, next } = TestHelper.createExpressMocks({ body });

    await controller.signin(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
