const { expect } = require('chai');

const seed = {
  email: "test@gmail.com",
  password: "abcd1234",
  username: "test"
};
let user = null;

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('authenticationAPIController');

  before(async () => {
    await service.deleteUsers({});
    const auth = await service.signup({ body: seed });
    user = await service.getUser(auth.userId);
  });

  it("Test successfull password update case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({
      currentUser: user,
      body: {
        password: "abcd1234",
        newPassword: "abcd12345",
        confirmPassword: "abcd12345"
      }
    });

    await controller.updatePassword(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test current password mismatch case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({
      currentUser: user,
      body: {
        password: "abcd123424",
        newPassword: "abcd12345",
        confirmPassword: "abcd12345"
      }
    });

    await controller.updatePassword(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test invalid new password case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({
      currentUser: user,
      body: {
        password: "abcd1234",
        newPassword: "abcd12345@",
        confirmPassword: "abcd12345@"
      }
    });

    await controller.updatePassword(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test new password and confirm password mismatch case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({
      currentUser: user,
      body: {
        password: "abcd1234",
        newPassword: "abcd12345",
        confirmPassword: "abcd1234"
      }
    });

    await controller.updatePassword(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
