const UserService = require('../../../services/UserService'); // USED ONLY FOR INTELLISENSE ISSUES
const UserDefinedException = require('../../../models/Business/Exeption/UserDefinedException')

class UserAPIController {

  /**
   * @param {UserService} userService 
   */
  constructor(userService) {
    this.userService = userService;

    this.details = this.details.bind(this);
    this.like = this.like.bind(this);
    this.unlike = this.unlike.bind(this);
    this.listByLikes = this.listByLikes.bind(this);
  }

  /**
   * @api {get} /user/:id User Details
   * @apiName UserDetails
   * @apiGroup Users
   * @apiVersion  0.1.0
   * 
   * @apiHeader {String} Authorization Authorization token received from /login
   * 
   * @apiParam {String} id  Unique identifier for user
   * 
   * @apiError {String} error Error message explaining the issue
   * 
   * @apiHeaderExample {json} Header-Example:
   * {
   *    "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YmFkZDQ1ZWQ0YTRlNDAwZTVhMjI1YjIiLCJpYXQiOjE1MzgxMzY4NTk0MzcsImhhc2giOiIyZjczMDYxZC0zNWQzLTRlMjAtODkzMS05NWZjOTQyMzkxNmMifQ.92knC28UoiYo_4ogkKj2nH6raHOSK4D40TdhfQb87l8"
   * }
   * 
   * @apiParamExample Request-Example:
   * GET /user/5c070e3ec4de4400cb1e0d10
   * 
   * @apiSuccessExample Success-Response:
   * Same as /me
   * 
   * @apiErrorExample Error-Response:
   * Same as /signup
   * 
   */
  async details(req, res, next) {
    const { id } = req.params;
    if (!id) throw new UserDefinedException("Param id is required", 404);

    try {
      const user = await this.userService.getUser(id);
      res.statusCode = 200;
      return res.json(user.asDTO());
    } catch (err) {
      next(err);
    }
  }

  /**
   * @api {patch} /user/:id/like User Like
   * @apiName UserLike
   * @apiDescription Used to like the specified user
   * @apiGroup Users
   * @apiVersion  0.1.0
   * 
   * @apiHeader {String} Authorization Authorization token received from /login
   * 
   * @apiParam {String} id  Unique identifier for user
   * 
   * @apiError {String} error Error message explaining the issue
   * 
   * @apiHeaderExample {json} Header-Example:
   * {
   *    "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YmFkZDQ1ZWQ0YTRlNDAwZTVhMjI1YjIiLCJpYXQiOjE1MzgxMzY4NTk0MzcsImhhc2giOiIyZjczMDYxZC0zNWQzLTRlMjAtODkzMS05NWZjOTQyMzkxNmMifQ.92knC28UoiYo_4ogkKj2nH6raHOSK4D40TdhfQb87l8"
   * }
   * 
   * @apiParamExample Request-Example:
   * PATCH /user/5c070e3ec4de4400cb1e0d10/like
   * 
   * @apiSuccessExample Success-Response:
   * Same as /me
   * 
   * @apiErrorExample Error-Response:
   * Same as /signup
   * 
   */
  async like(req, res, next) {
    const { id } = req.params;
    if (!id) throw new UserDefinedException("Param id is required", 404);

    try {
      const user = await this.userService.likeUser(req.currentUser.id, id);
      res.statusCode = 200;
      return res.json(user.asDTO());
    } catch (err) {
      next(err);
    }
  }

  /**
   * @api {patch} /user/:id/unlike User Unlike
   * @apiName UserUnlike
   * @apiDescription Used to unlike the specified user
   * @apiGroup Users
   * @apiVersion  0.1.0
   * 
   * @apiHeader {String} Authorization Authorization token received from /login
   * 
   * @apiParam {String} id  Unique identifier for user
   * 
   * @apiError {String} error Error message explaining the issue
   * 
   * @apiHeaderExample {json} Header-Example:
   * {
   *    "Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YmFkZDQ1ZWQ0YTRlNDAwZTVhMjI1YjIiLCJpYXQiOjE1MzgxMzY4NTk0MzcsImhhc2giOiIyZjczMDYxZC0zNWQzLTRlMjAtODkzMS05NWZjOTQyMzkxNmMifQ.92knC28UoiYo_4ogkKj2nH6raHOSK4D40TdhfQb87l8"
   * }
   * 
   * @apiParamExample Request-Example:
   * PATCH /user/5c070e3ec4de4400cb1e0d10/unlike
   * 
   * @apiSuccessExample Success-Response:
   * Same as /me
   * 
   * @apiErrorExample Error-Response:
   * Same as /signup
   * 
   */
  async unlike(req, res, next) {
    const { id } = req.params;
    if (!id) throw new UserDefinedException("Param id is required", 404);

    try {
      const user = await this.userService.unlikeUser(req.currentUser.id, id);
      res.statusCode = 200;
      return res.json(user.asDTO());
    } catch (err) {
      next(err);
    }
  }

  /**
   * @api {get} /most-liked User List
   * @apiName UserList
   * @apiDescription Used to list users
   * @apiGroup Users
   * @apiVersion  0.1.0
   * 
   * @apiHeader {String} [Authorization] Authorization token received from /login, if provided authenticated user is excluded from list
   * 
   * @apiParam {Number} [page] Page index, is not zero-based!
   * 
   * @apiSuccess (200) {Object[]} docs Collection of users
   * @apiSuccess (200) {String} docs.id Unique identifier for user
   * @apiSuccess (200) {String} docs.email Unique email address for user
   * @apiSuccess (200) {String} docs.username Unique username
   * @apiSuccess (200) {String} [docs.password] User password, always null
   * @apiSuccess (200) {String} [docs.tokenHash] User token hash, always null
   * @apiSuccess (200) {String} [docs.passwordResetToken] User password reset token, always null
   * @apiSuccess (200) {String} [docs.passwordResetSentAt] Password reset send date, always null
   * @apiSuccess (200) {String[]} docs.userLikes Identifiers of users that liked current user
   * @apiSuccess (200) {Number} docs.totalLikes Total number of likes from others for current user
   * @apiSuccess (200) {String} docs.created Date when the user is registered
   * @apiSuccess (200) {Number} totalDocs Total number of items in query
   * @apiSuccess (200) {Number} limit Page size
   * @apiSuccess (200) {Boolean} hasPrevPage Indicates if the collection has pervious page
   * @apiSuccess (200) {Boolean} hasNextPage Indicates if the collection has next page
   * @apiSuccess (200) {Number} page Indicates current page
   * @apiSuccess (200) {Number} totalPages Total number of pages
   * @apiSuccess (200) {Number} [prevPage] Indicates previous page, Nullable
   * @apiSuccess (200) {Number} [nextPage] Indicates next page, Nullable
   * 
   * @apiError {String} error Error message explaining the issue
   * 
   * 
   * @apiParamExample Request-Example:
   * GET /most-liked?page=1
   * 
   * @apiSuccessExample Success-Response:
   * {
   *    "docs": [
   *        {
   *            "id": "5c071b97dcf6820403f6d97b",
   *            "username": "vigan.abd",
   *            "email": "vig.an.abd@gmail.com",
   *            "password": null,
   *            "tokenHash": null,
   *            "passwordResetToken": null,
   *            "passwordResetSentAt": null,
   *            "userLikes": [
   *                "5c072009b85d58068d8d2dd7"
   *            ],
   *            "totalLikes": 1,
   *            "created": "2018-12-05T00:28:07.533Z"
   *        },
   *        {
   *            "id": "5c085c669b12c7002a8321eb",
   *            "username": "vigan2",
   *            "email": "vigan2@mail.com",
   *            "password": null,
   *            "tokenHash": null,
   *            "passwordResetToken": null,
   *            "passwordResetSentAt": null,
   *            "userLikes": [],
   *            "totalLikes": 0,
   *            "created": "2018-12-05T23:16:54.829Z"
   *        }
   *    ],
   *    "totalDocs": 2,
   *    "limit": 10,
   *    "hasPrevPage": false,
   *    "hasNextPage": false,
   *    "page": 1,
   *    "totalPages": 1,
   *    "prevPage": null,
   *    "nextPage": null
   * }
   * 
   * @apiErrorExample Error-Response:
   * Same as /signup
   * 
   */
  async listByLikes(req, res, next) {
    const { page = 1 } = req.query;

    try {
      const collection = await this.userService.listByLikes(page, req.currentUser ? req.currentUser.id : null);
      res.statusCode = 200;
      collection.docs = collection.docs.map(x => x.asDTO());
      return res.json(collection);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = UserAPIController;