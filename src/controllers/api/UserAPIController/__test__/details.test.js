const { expect } = require('chai');

const testUserData = {
  body: {
    email: "test@gmail.com",
    password: "abcd1234",
    username: "test"
  }
};
let userId = null;

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('userAPIController');

  before(async () => {
    await service.deleteUsers({});
    userId = await service.signup(testUserData).then(res => res.userId);
  });

  it("Test user exists case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ params: { id: userId } });

    await controller.details(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test user not to exist case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ params: { id: "1234" } });
    await controller.details(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
