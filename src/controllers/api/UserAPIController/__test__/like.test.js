const { expect } = require('chai');

const seed = [
  { email: "user1@test.com", password: "abcd1234", username: "user1" },
  { email: "user2@test.com", password: "abcd1234", username: "user2" }
];

let users = [];

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('userAPIController');

  before(async () => {
    await service.deleteUsers({});
    for (let i = 0; i < seed.length; i++) {
      const auth = await service.signup({ body: seed[i] });
      const user = await service.getUser(auth.userId);
      users.push(user);
    }
  });

  it("Test user can like case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ currentUser: users[0], params: { id: users[1].id } });
    await controller.like(req, res, next);
    const data = JSON.parse(res._getData());
    return expect(data).to.exist;
  });

  it("Test user already like case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ currentUser: users[0], params: { id: users[1].id } });
    await controller.like(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test self like case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ currentUser: users[0], params: { id: users[0].id } });
    await controller.like(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test inexistent users like case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ currentUser: users[0], params: { id: "ssss" } });
    await controller.like(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  it("Test unauthorized like case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ params: { id: users[1].id } });
    await controller.like(req, res, next);
    return expect(next.calledOnce).to.be.true;
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
