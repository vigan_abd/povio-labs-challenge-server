const { expect } = require('chai');

const seed = [
  { email: "user1@test.com", password: "abcd1234", username: "user1" },
  { email: "user2@test.com", password: "abcd1234", username: "user2" },
  { email: "user3@test.com", password: "abcd1234", username: "user3" },
];

let users = [];

module.exports = (container) => {
  const service = container.resolve('userService');
  const controller = container.resolve('userAPIController');

  before(async () => {
    await service.deleteUsers({});
    for (let i = 0; i < seed.length; i++) {
      const auth = await service.signup({ body: seed[i] });
      const user = await service.getUser(auth.userId);
      users.push(user);
    }
    await service.likeUser(users[0].id, users[1].id);
    await service.likeUser(users[0].id, users[2].id);
    await service.likeUser(users[2].id, users[1].id);
  });

  it("Test return data correct case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks();

    await controller.listByLikes(req, res, next);
    const data = JSON.parse(res._getData());

    await expect(data.docs).to.exist;
    await expect(data.docs.length).to.be.equal(3);

    for (let i = 1; i < data.docs.length; i++) {
      await expect(data.docs[i - 1].totalLikes).to.be.gte(data.docs[i].totalLikes);
    };
    return expect(data.totalPages).to.be.equal(1);
  });

  it("Test return page correct case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ query: { page: 2 } });

    await controller.listByLikes(req, res, next);
    const data = JSON.parse(res._getData());

    await expect(data.docs).to.exist;
    await expect(data.docs.length).to.be.equal(0);
    return expect(data.totalPages).to.be.equal(1);
  });

  it("Test filtering auth user case", async () => {
    const { req, res, next } = TestHelper.createExpressMocks({ currentUser: users[0] });

    await controller.listByLikes(req, res, next);
    const data = JSON.parse(res._getData());

    await expect(data.docs.length).to.be.equal(2);
    return expect(data.docs.filter(x => x.id == users[0].id).length).to.be.equal(0);
  });

  after(async () => {
    await service.deleteUsers({});
  });
};
