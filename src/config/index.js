const envFile = require('node-env-file');
const path = require('path');
const env = require('../helpers/EnvHelper');

try {
  envFile(path.join(__dirname, process.env.NODE_ENV + '.env'));
} catch (e) {
  console.log(`No config file found for ${process.env.NODE_ENV}`);
}

const APP_ENV = env('APP_ENV', process.env.NODE_ENV || 'development');
const HOST = env('HOST', '0.0.0.0');

const config = {
  // ENV
  APP_NAME: env('APP_NAME', 'Node JS'),
  APP_ENV: APP_ENV,
  APP_DEBUG: env('APP_DEBUG', true),
  PORT: env('PORT', process.env.PORT || '3001'),
  HOST: HOST,
  ROOT: `${__dirname}/..`,

  // SECURITY
  JWT_SECRET: env('JWT_SECRET', 'notsosecure'),
  JWT_EXPIRE_TIME: env('JWT_EXPIRE_TIME', 3600), //seconds

  // REQUEST
  REQ_PAYLOAD_LIMIT: env('REQ_PAYLOAD_LIMIT', '50mb'),

  // LOG
  LOG_LEVEL: env('LOG_LEVEL', 'silly'),

  // DB
  DB_CONNECTION_STRING: `mongodb://${env('DB_HOST', '127.0.0.1')}:${env('DB_PORT', 27017)}/${env('DB_DATABASE', "admin")}`,
};


module.exports = config;