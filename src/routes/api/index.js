const express = require("express");
const router = express.Router();
const RouteBuilder = require("simple-express-route-builder");

const register = container => {
  // MIDDLEWARES
  const authMiddleware = container.resolve("AuthMiddleware");

  // CONTROLLERS
  const authenticationAPIController = container.resolve("authenticationAPIController");
  const userAPIController = container.resolve("userAPIController");

  const builder = new RouteBuilder("", router);

  builder.use((group, action) =>
    group("/signup", [
      action("POST", authenticationAPIController.signup)
    ])
  );

  builder.use((group, action) =>
    group("/login", [
      action("POST", authenticationAPIController.signin)
    ])
  );

  builder.use((group, action) =>
    group("/me", [authMiddleware.authHandler], [
      action("GET", authenticationAPIController.currentUser),
      group("/update-password", [
        action("PATCH", authenticationAPIController.updatePassword)
      ])
    ])
  );

  builder.use((group, action) =>
    group("/user/:id", [
      action("GET", userAPIController.details),
      group("/like", [authMiddleware.authHandler], [
        action("PATCH", userAPIController.like)
      ]),
      group("/unlike", [authMiddleware.authHandler], [
        action("PATCH", userAPIController.unlike)
      ])
    ])
  );

  builder.use((group, action) =>
    group("/most-liked", [authMiddleware.optionalAuthHandler], [
      action("GET", userAPIController.listByLikes)
    ])
  );

  return router;
};

module.exports = register;
