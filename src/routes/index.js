const api = require('./api');
const event = require('./event');
const web = require('./web');

module.exports = {
  api,
  event,
  web,
};
