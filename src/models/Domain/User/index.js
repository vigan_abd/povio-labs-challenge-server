const Joi = require('joi');

const ModelBase = require('../ModelBase');
const DbQuery = require('./schema');

class User extends ModelBase {

  /**
   * @param {{id: String, username: String, email: String, 
          password: String, tokenHash: String, passwordResetToken: String, passwordResetSentAt: Date,
          userLikes: String[], totalLikes: Number, created: Date
      }}
   */
  constructor({ id, username, email, password, tokenHash, passwordResetToken, passwordResetSentAt, userLikes, totalLikes, created }) {
    super();
    this.id = id;
    this.username = username;
    this.email = email;
    this.password = password;
    this.tokenHash = tokenHash;
    this.passwordResetToken = passwordResetToken;
    this.passwordResetSentAt = passwordResetSentAt;
    this.userLikes = userLikes;
    this.totalLikes = totalLikes;
    this.created = created;


    this.rules = Joi.object().keys({
      id: Joi.optional().allow(null),
      username: Joi.string().required().min(3),
      email: Joi.string().required().email(),
      password: Joi.string().required(),
      tokenHash: Joi.string().required(),
      passwordResetToken: Joi.string().optional().allow(null),
      passwordResetSentAt: Joi.date().optional().allow(null),
      created: Joi.date().optional().allow(null),
      userLikes: Joi.array().items(Joi.string()).optional().allow(null),
      totalLikes: Joi.number().optional().allow(null)
    });
  }

  asDTO() {
    const user = this.toJSON();
    user.password = null;
    user.tokenHash = null;
    user.passwordResetToken = null;
    user.passwordResetSentAt = null;
    return user;
  }

  static get DbQuery() {
    return DbQuery;
  }

}

module.exports = User;