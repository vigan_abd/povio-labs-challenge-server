#!/bin/bash
APP_CONTAINER="povio_labs_challenge_server"
DB_CONTAINER="povio_labs_challenge_db"
DOCKER_CMD="docker"
DOCKER_COMPOSE_CMD="docker-compose"
echo "Starting Povio Labs - Challenge - App >>>"

$DOCKER_CMD stop $APP_CONTAINER
$DOCKER_CMD stop $DB_CONTAINER