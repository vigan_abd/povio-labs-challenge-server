#!/bin/bash
APP_CONTAINER="povio_labs_challenge_server"
DB_CONTAINER="povio_labs_challenge_db"
APP_IMG="povio_labs_challenge_server:1.0"
DOCKER_CMD="docker"
DOCKER_COMPOSE_CMD="docker-compose"

echo "Uninstalling Povio Labs - Challenge - Server <<<"
$DOCKER_CMD stop $APP_CONTAINER
$DOCKER_CMD rm $APP_CONTAINER
$DOCKER_CMD rmi $APP_IMG

$DOCKER_CMD stop $DB_CONTAINER
$DOCKER_CMD rm $DB_CONTAINER