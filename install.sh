#!/bin/bash
set -e
DOCKER_CMD="docker"
DOCKER_COMPOSE_CMD="docker-compose"
DOCKET_COMPOSE_FILE="docker-compose.prod.yml"

APP_CONTAINER="povio_labs_challenge_server"
DB_CONTAINER="povio_labs_challenge_db"

## BETTER IF CREDENTIALS PROVIDED VIA CLI ARGS
DB_USER="root"
DB_PASS="root"
DB_NAME="povio_labs_challenge"
DB_TEST_NAME="povio_labs_challenge_test"

if [ "$1" == "--dev" ]; then
  DOCKET_COMPOSE_FILE="docker-compose.yml"
fi

echo "Installing Povio Labs - Challenge - Server >>>"

## BOOT DB MICROSERVICE
$DOCKER_COMPOSE_CMD -f $DOCKET_COMPOSE_FILE up -d $DB_CONTAINER;

## BUILD SERVER MICROSERVICE
$DOCKER_COMPOSE_CMD -f $DOCKET_COMPOSE_FILE build $APP_CONTAINER;

## CREATE NEW DATABASES
$DOCKER_CMD exec -it $DB_CONTAINER mongo $DB_NAME -u $DB_USER -p $DB_PASS --authenticationDatabase admin --eval "db.bootLogs.insert({ message: '$DB_NAME database created on ' + new Date() })";
$DOCKER_CMD exec -it $DB_CONTAINER mongo $DB_TEST_NAME -u $DB_USER -p $DB_PASS --authenticationDatabase admin --eval "db.bootLogs.insert({ message: '$DB_TEST_NAME database created on ' + new Date() })";

## SET SERVER MICROSERVICE
if [ "$1" == "--dev" ]; then
  $DOCKER_COMPOSE_CMD -f $DOCKET_COMPOSE_FILE up $APP_CONTAINER;
else
  if [ ! -f "./src/config/production.env" ]; then
    cp ./src/config/development.env ./src/config/production.env
  fi
  $DOCKER_COMPOSE_CMD -f $DOCKET_COMPOSE_FILE up -d $APP_CONTAINER;
fi